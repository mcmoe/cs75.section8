<?php
/**********************************************************************
 * buy.php = For Cross-Site Request Forgery attach
 **********************************************************************/

// database connection 
$dsn = 'mysql:host=localhost;dbname=jharvard_section8';
$db_user = 'jharvard';
$db_pass = 'crimson';
$dbh = new PDO($dsn, $db_user, $db_pass);

session_start();
?>

<html>
    <head>
        <title>Cross-Site Request Forgery</title>
    </head>
    <body>
<?php
// handle database inserts
if (isset($_SESSION['id']) &&
    isset($_GET['symbol']) &&
    isset($_GET['shares']))
{
    $stmt = $dbh->prepare('INSERT INTO portfolios (id,symbol,shares)
                           VALUES (:id,:symbol,:shares)');
    $stmt->bindValue(':id',$_SESSION['id']);
    $stmt->bindValue(':symbol',$_GET['symbol']);
    $stmt->bindValue(':shares',$_GET['shares']);
    $stmt->execute();
    echo "<p>You just bought {$_GET['shares']} shares of {$_GET['symbol']}!</p>";
}
?>
    </body>
</html>
